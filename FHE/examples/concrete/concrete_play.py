from concrete import fhe
import numpy as np


# These are the functions we want to compile to their fhe equivalent.
def multiply(x, y):
    return x * y


def add(x, y):
    return x + y


def square(a):
    return a * a


def example_function_to_fhe():
    domain = [(0, 0), (1, 1), (2, 4), (5, 3), (3, 5), (11, 10)]

    print(f"Passing the function to the compiler")
    compiler = fhe.Compiler(multiply, {"x": "encrypted", "y": "encrypted"})

    print(f"Compiling...")
    circuit = compiler.compile(domain)

    print(f"Generating keys...")
    circuit.keygen()
    examples = [(3, 4), (1, 2), (7, 7), (0, 0)]

    for (x, y) in examples:
        print(f'x: {x}, y: {y}')
        #encrypted_example = circuit.encrypt()
        enc = circuit.encrypt(x, y)
        print(f'enc_x: {enc}')
        encrypted_result = circuit.run(enc)
        result = circuit.decrypt(encrypted_result)
        print(f"Evaluation of {' * '.join(map(str, (x,y)))} homomorphically = {result}")


def generic_compile(func, domain):
    compiler = fhe.Compiler(func, {"x": "encrypted", "y": "encrypted"})
    circuit = compiler.compile(domain)

    return circuit


def branchless_func(x, y):
    a = (x % 2)
    return (x + y) * a + (1 - a) * (x * y)


def branching_function(x, y):
    if (x % 2 == 0):
        return x + y

    return x * y

if __name__ == "__main__":
    print(f'Passing the function to the compiler')
    singles = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
    pairs = [(0, 0), (1, 1), (2, 4), (5, 3), (3, 5), (11, 10), (0, 1), (14, 16)]

    adder_circuit = generic_compile(add, pairs)

    multiply_circuit = generic_compile(multiply, pairs)
    print(f'Generated two Circuits.')
    # adder_circuit.draw() # requires full version of concrete :S
    print(f"Graphs:\n #{adder_circuit}")
    print(f"Graphs:\n #{multiply_circuit}")

    print('\n======================\n')
    print(f'Checking the keys of both circuits are the same')
    adder_keys = adder_circuit.keys

    multiply_circuit.keys = adder_keys
    multiply_keys = multiply_circuit.keys

    # still no idea how key generation works under the hood.
    if adder_keys == multiply_keys:
        print('The keys are identical')
    else:
        print('The keys are different')

    print(f'Input a number x: ')
    test_a = int(input())

    print(f'Input another number y: ')
    test_b = int(input())

    print(f'encrypting input values x: #{test_a}, y: {test_b}...\n')
    enc = adder_circuit.encrypt(test_a, test_b)
    enc2 = multiply_circuit.encrypt(test_a, test_b)
    print(f'enc_a: {enc}')
    enc_result = adder_circuit.run(enc)
    mult_res = multiply_circuit.run(enc2)
    result = adder_circuit.decrypt(enc_result)
    result2 = multiply_circuit.decrypt(mult_res)
    result3 = adder_circuit.decrypt(mult_res)

    print(f'result: {result}')
    print('note to self: you cannot stack easily functions, ie, add(10, multiply(20,10)) is not that easy to do... ')
    print('note to self: still not sure how keygen is working or how many classes of graphs exists and do they know about each other')

