from sklearn.datasets import load_digits
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from torch import nn
from concrete.ml.sklearn import NeuralNetClassifier

print("hello!")
print("We are loading the neural_networks digits from sklearn. Each pixel apparently has 0 -> 16 (17) possible values")
print("So we don't really need to downsample anything")

mnist = load_digits()

n_samples = len(mnist.data)
sample_dimension = len(mnist.data[0])

#for i in range(n_samples):
    #print(max(neural_networks.data[i]))

print("Number of Samples: %d" %+ n_samples)

# neural_networks.data is the inputs (x), neural_networks.target is the labels array (y)
x_train, x_test, y_train, y_test = train_test_split(mnist.data, mnist.target, test_size=15, shuffle=True)

params = {
    "module__n_layers": 2,
    "module__n_w_bits": 4,
    "module__n_a_bits": 4,
    "module__n_hidden_neurons_multiplier": 0.5,
    "module__activation_function": nn.ReLU,
    "max_epochs": 15,
}

model = NeuralNetClassifier(**params)

# plaintext training
model.fit(x_train, y_train)

# prediction in plaintext
y_pred = model.predict(x_test)

print("Plaintext training and predictions")

for i in range(len(y_pred)):
    print(f"Predicted: {y_pred[i]} & Actual: {y_test[i]}")

print(" ----------------------------- ")
print(" Compiling the model and running in fhe mode")
# compilation
fhe_circuit = model.compile(x_train[:25])

print(fhe_circuit)
print(f"Circuit of {fhe_circuit.graph.maximum_integer_bit_width()}-bits")

predictions = model.predict(x_test[:20], fhe="execute")
y_test_subset = y_test[:20]

print("Model inference while encrypted")
print("predictions are LEFT, and actual values are RIGHT")
print(predictions, "\t", y_test_subset)

print("----------------------------------")
print("Now doing the prediction and encryption and stuff manually")

quantized_x = model.quantize_input(x_test[0])
quantized_x = quantized_x.reshape(1, -1)
encrypted_x = model.fhe_circuit.encrypt(quantized_x)
quantized_encrypted_prediction = model.fhe_circuit.run(encrypted_x)
quantized_prediction = model.fhe_circuit.decrypt(quantized_encrypted_prediction)
pred_probability = model.post_processing(model.dequantize_output(quantized_prediction))

idx_of_max = pred_probability.argmax()

print("The final result of manually doing things")
print(idx_of_max, y_test[0])

# old stuff for me to checkout the actual data.
fig, axes = plt.subplots(2, 10, figsize=(16, 6))

for i in range(20):
    axes[i // 10, i % 10].imshow(mnist.images[i], cmap='gray')
    axes[i // 10, i % 10].axis('off')
    axes[i // 10, i % 10].set_title(f"label: {mnist.target[i]}")

plt.tight_layout()
#plt.show()

