# Encrypted Training with a Logistic Regression model on two classes.
# Using a subset of the iris dataset (just 2 classes of flowers instead of all)

# Import dataset libraries and util functions
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import ListedColormap
from matplotlib.lines import Line2D
from sklearn import datasets
from sklearn.linear_model import SGDClassifier as SklearnSGDClassifier
from sklearn.preprocessing import MinMaxScaler

from concrete.ml.sklearn import SGDClassifier

N_ITERATIONS = 15
RANDOM_STATE = 42


def plot_decision_boundary(clf, X, y, n_iterations, title="Decision Boundary", accuracy=None):
    # Create a mesh to plot the decision boundaries
    x_min, x_max = X[:, 0].min() - 0.1, X[:, 0].max() + 0.1
    y_min, y_max = X[:, 1].min() - 0.1, X[:, 1].max() + 0.1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, 0.01), np.arange(y_min, y_max, 0.01))

    # Predictions to get the decision boundary
    Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)

    # Define red and blue color map
    cm_bright = ListedColormap(["#FF0000", "#0000FF"])

    # Plotting the results
    plt.figure(figsize=(10, 6))
    plt.contourf(xx, yy, Z, alpha=0.3, cmap=cm_bright)
    plt.scatter(X[:, 0], X[:, 1], c=y, edgecolor="k", cmap=cm_bright)
    plt.title(
        f"{title} (Iterations: {n_iterations}, Accuracy: {accuracy})\n "
        f"Learned weights: {clf.coef_[0][0]:.3f}, {clf.coef_[0][1]:.3f}, "
        f"{clf.intercept_.reshape((-1,))[0]:.3f} "
    )
    plt.xlabel("Feature 1")
    plt.ylabel("Feature 2")

    # Create a custom legend
    legend_elements = [
        Line2D(
            [0],
            [0],
            marker="o",
            color="w",
            label="Class 0",
            markerfacecolor="#FF0000",
            markersize=10,
        ),
        Line2D(
            [0],
            [0],
            marker="o",
            color="w",
            label="Class 1",
            markerfacecolor="#0000FF",
            markersize=10,
        ),
    ]
    plt.legend(handles=legend_elements, loc="upper right")

    plt.show()


# Load the Iris dataset
Xfull, y = datasets.load_iris(return_X_y=True)
Xfull = MinMaxScaler(feature_range=[-1, 1]).fit_transform(Xfull)

# Select petal length and petal width for visualization
X = Xfull[:, 2:4]  # Petal length and petal width

# Filter the dataset for binary classification (Versicolor and Virginica)
# These correspond to target labels 1 and 2 in the Iris dataset
binary_filter = (y == 1) | (y == 2)
X_binary = X[binary_filter]
Xfull_binary = Xfull[binary_filter]
y_binary = y[binary_filter] - 1             # Why - 1? to shift the values from 2 1 to 1 0

# Train an SGDClassifier on the binary dataset


sgd_clf_binary = SklearnSGDClassifier(random_state=RANDOM_STATE, max_iter=N_ITERATIONS)
sgd_clf_binary.fit(X_binary, y_binary)
y_pred = sgd_clf_binary.predict(X_binary)
accuracy = (y_pred == y_binary).mean()
plot_decision_boundary(
    sgd_clf_binary,
    X_binary,
    y_binary,
    n_iterations=N_ITERATIONS,
    accuracy=accuracy,
    title="Scikit-Learn decision boundary",
)

parameters_range = (-1.0, 1.0)

## This is the concrete-ml class that inherits/overrides the sgd-classifier from sci-kit learn
sgd_clf_binary_fhe = SGDClassifier(
    random_state=RANDOM_STATE,
    max_iter=N_ITERATIONS,
    fit_encrypted=True,
    parameters_range=parameters_range,
    verbose=True,
)

print("this is training")
# Fit on encrypted data
sgd_clf_binary_fhe.fit(X_binary, y_binary, fhe="execute")

# The weights are decrypted at the end of the `fit` call. Use the clear weights here
# to evaluate accuracy on clear data
y_pred = sgd_clf_binary_fhe.predict(X_binary)

# Evaluate the decrypted weights on encrypted data
sgd_clf_binary_fhe.compile(X_binary)
y_pred_fhe = sgd_clf_binary_fhe.predict(X_binary, fhe="execute")

# Check that the same result is obtained when applying
# the decrypted model on clear data and on encrypted data
# Linear classifiers are 100% correct on encrypted data compared to execution on clear data
assert np.all(y_pred == y_pred_fhe) # Abdulilah: Not sure why that statement is true, 100% correct?

accuracy = (y_pred == y_binary).mean()

plot_decision_boundary(
    sgd_clf_binary_fhe,
    X_binary,
    y_binary,
    n_iterations=N_ITERATIONS,
    accuracy=accuracy,
    title="Concrete ML (training on encrypted data with FHE) decision boundary",
)

