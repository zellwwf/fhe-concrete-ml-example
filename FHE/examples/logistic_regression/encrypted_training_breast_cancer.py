# ======================= Breast Cancer With Logistic Regression =============================================
import matplotlib.pyplot as plt
import numpy as np
from sklearn import datasets
from sklearn.linear_model import SGDClassifier as SklearnSGDClassifier
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split

from concrete.ml.sklearn import SGDClassifier

N_ITERATIONS = 15
RANDOM_STATE = 42


X2, y2 = datasets.load_breast_cancer(return_X_y=True)
x2_train, x2_test, y2_train, y2_test = train_test_split(X2, y2, test_size=0.3, stratify=y2)

scaler = MinMaxScaler(feature_range=[-1, 1])
x2_train = scaler.fit_transform(x2_train)
x2_test = scaler.transform(x2_test)

rng = np.random.default_rng(RANDOM_STATE)
perm = rng.permutation(x2_train.shape[0])
x2_train = x2_train[perm, ::]
y2_train = y2_train[perm]

parameters_range = (-1.0, 1.0)

sklearn_sgd = SklearnSGDClassifier()
sklearn_sgd.fit(x2_train, y2_train)
accuracy_sk = np.mean(sklearn_sgd.predict(x2_test) == y2_test)
print(f"Sklearn clear accuracy: {accuracy_sk*100:.2f}%")

sgd_clf_binary_simulate = SGDClassifier(
    random_state=RANDOM_STATE,
    max_iter=N_ITERATIONS,
    fit_encrypted=True,
    parameters_range=parameters_range,
)

# Train with simulation on the full dataset
sgd_clf_binary_simulate.fit(x2_train, y2_train, fhe="simulate")

# Measure accuracy on the test set using simulation
sgd_clf_binary_simulate.compile(x2_train)
y_pred_fhe = sgd_clf_binary_simulate.predict(x2_test, fhe="simulate")
accuracy = (y_pred_fhe == y2_test).mean()
print(f"Full encrypted fit (simulated) accuracy {accuracy*100:.2f}%")

# To measure accuracy after every batch initialize the SGDClassifier with warm_start=True
# which keeps the weights obtained with previous batches

sgd_clf_binary_simulate = SGDClassifier(
    random_state=RANDOM_STATE,
    max_iter=N_ITERATIONS,
    fit_encrypted=True,
    parameters_range=parameters_range,
    warm_start=True,
)

batch_size = sgd_clf_binary_simulate.batch_size

# Go through the training batches
acc_history = []
for idx in range(x2_train.shape[0] // batch_size):
    batch_range = range(idx * batch_size, (idx + 1) * batch_size)
    x_batch = x2_train[batch_range, ::]
    y_batch = y2_train[batch_range]

    # Fit on a single batch with partial_fit
    sgd_clf_binary_simulate.partial_fit(x_batch, y_batch, fhe="simulate")

    # Measure accuracy of the model with FHE simulation
    sgd_clf_binary_simulate.compile(x2_train)
    y_pred_fhe = sgd_clf_binary_simulate.predict(x2_test, fhe="simulate")
    accuracy = (y_pred_fhe == y2_test).mean()
    acc_history.append(accuracy)

# Plot the evolution of accuracy throughout the training process
fig = plt.figure()
plt.plot(acc_history)
plt.title(f"Accuracy evolution on breast-cancer. Final accuracy {acc_history[-1]*100:.2f}%")
plt.xlabel("Batch number")
plt.ylabel("Accuracy")
plt.grid(True)
plt.show()

