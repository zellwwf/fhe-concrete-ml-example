from sklearn.datasets import load_digits
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from torch import nn
from concrete.ml.sklearn import SGDClassifier
from sklearn.preprocessing import MinMaxScaler

print("Training on Encrypted Data")

mnist = load_digits()

n_samples = len(mnist.data)
sample_dimension = len(mnist.data[0])

# neural_networks.data is the inputs (x), neural_networks.target is the labels array (y)
x_train, x_test, y_train, y_test = train_test_split(mnist.data, mnist.target, test_size=15, shuffle=True)

scaler = MinMaxScaler(feature_range=[-1, 1])
x2_train = scaler.fit_transform(x_train)
x2_test = scaler.transform(x_test)

model = SGDClassifier(
    random_state=42,
    max_iter=50,
    #fit_encrypted=True,
    warm_start=True,
    #parameters_range=[-1, 1],
)

model.fit(x2_train, y_train)

print('Training finished')

model.predict(x2_test)
# plaintext training
