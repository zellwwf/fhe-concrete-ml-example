# README

the code builds a small 2 hidden layer neural network running on concrete-ml. It will automatically use the mnist dataset.
The dataset is obtained from sklearn directly, as I spent lots of time downloading bad ones, I don't know why I did not think of that earlier.


### How to run this?
Well, the dockerfile is not complete, so you are better off running this in 
a virtualenv in python3, I attached the requirements.txt for easier setup.
If you need help setting up python 3.8, please see `getting-started.md`


---

### Work notes

 docker build -t concrete_app:latest -f Dockerfile .
