# Use the zama's image
FROM zamafhe/concrete-ml:latest

WORKDIR /app

COPY . /app/

RUN ls -a /

RUN ls -a /app

VOLUME /app/data

CMD ["python", "/app/FHE/examples/concrete_play.py"]

