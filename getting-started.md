# Getting Started With Python

This covers Ubuntu 22.04 - and installing python 3.8, but it will work for most python 3> versions.

#### Download & Setup Python, Pip and virtualenv

Ubunutu 22.04.4 comes with python3, ships with the system.
We usually do not want to mess with that python. If your curious [for reasons why](https://askubuntu.com/questions/1397938/terminal-not-opening-after-changing-python-version)...

Just for fun, check your python version:


`python3 --version`
`>> Python 3.10.12`

Regardless of the outcome, we need to install version 3.8, since that's the latest python Concrete-ML supports.


### A) Installing an older version of python3.
We can build python3 from source code; not covered in this guide. Or we can use the deadsnakes apt repository.

1. Add the deadsnakes repo

   `sudo add-apt-repository ppa:deadsnakes/ppa`
2. Update apt repo lists

   `sudo apt update`
3. Download your version of python (in our case 3.8)

```shell
   sudo apt-get python3.8 # this installs a different version of python
   
   sudo apt install python3.8-distutils # This installs extra stuff, including PIP :)
```

#### Mistakes I Made In these steps:
what I did:
1. I `sudo apt-get python3.9` it actually got something, but wasn't python,
   cuz I needed to add the deadsnakes repo that has older version of python

If you see any of the following libraries being installed, you did something wrong, remove them and make sure you add the deadsnakes repo.
```
>> Note, selecting 'python3.9-llfuse' for regex 'python3.9'
>> Note, selecting 'python3-llfuse' instead of 'python3.9-llfuse'
```

2. Forgetting to specify a python version for virtual env 

`virtualenv --python=path/to/python3.8 venv`

### Installing Pip, python's package manager

If you downloaded python3.8 from the steps above you should have pip installed, 
to ensure that ur python has pip:

``
python3.8 -m pip --version
``

it should output something like
``
pip 22.0.2 from /usr/lib/python3/dist-packages/pip (python 3.7)
``


### Setting up your virtual environment
1. Downloading and installing virtualenv (Python package isolator)

`python -m pip install --user virtualenv`

2. Create a new environment folder for this project.

`virtualenv [ur envname]`
`virtualenv --python=path/to/python3.9 venv`
3. Activate the environment

`source venv/bin/activate`
this should switch the python interpreter, and it will show you the name of the venv ur currently in on the terminal, like this
```shell
zellwwf@ub20-nera:~/Desktop/fhe-concrete-ml-example$ source venv/bin/activate
(venv) zellwwf@ub20-nera:~/Desktop/fhe-concrete-ml-example$ 

```
To deactivate the venv, just type `deactivate`.

4. Install the requirements.txt via pip (point to the requirements.txt that's in this folder)
`pip install -r requirements.txt`


### Running the example
If all the installation steps are working, you can run the examples via


```shell

python3.8 [filename.py] 
```